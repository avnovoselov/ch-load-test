<?php

namespace App\Command;

use App\Infrastructure\CHConnection;
use App\Util\Faker;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(name: 'app:fill-database', aliases: ['afdb'])]
class FillDatabase extends Command
{
    private int $batchSize;

    private int $batchCount;

    public function __construct(
        private readonly CHConnection $connection,
        private readonly Faker        $faker,
        private readonly ParameterBagInterface $parameterBag,
    )
    {
        $this->batchSize = $this->parameterBag->get('batch_size');
        $this->batchCount = $this->parameterBag->get('batch_count');

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $progressBar = new ProgressBar($output, $this->batchCount);
        $progressBar->start();

        $lastId = $this->getMaxId();
        $columns = ["id", "user_id", "created_at", "amount", "income"];

        $output->writeln("");
        $i = $this->batchCount;

        while ($i--) {
            $j = $this->batchSize;
            $batch = [];

            while ($j--) {
                $amount = $this->faker->provideAmount();

                $batch[] = [
                    ++$lastId,
                    $this->faker->provideUserId(),
                    $this->faker->provideDateTime(),
                    $amount,
                    (int) $this->faker->isIncome($amount)
                ];
            }

            $this->connection->insert("operations", $batch, $columns);

            $progressBar->advance();
        }

        return self::SUCCESS;
    }

    private function getMaxId(): int
    {
        return rand(1, 100_000_000_000);
    }
}
