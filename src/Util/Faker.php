<?php

namespace App\Util;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Faker
{
    private int $userCount;

    private DateTimeInterface $from;

    private DateTimeInterface $to;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->userCount = $parameterBag->get('user_count');
        $this->to = new DateTimeImmutable();
        $this->from = $this->to->sub(new DateInterval('P3M'));
    }

    public function provideUserId(): int
    {
        return rand(1, $this->userCount);
    }

    public function provideAmount(): int
    {
        $amount = rand(-10000, 10000);

        return $amount * 100;
    }

    public function isIncome(int $amount): bool
    {
        return $amount > 0;
    }

    public function provideDateTime(): DateTimeInterface
    {
        $i = rand($this->from->getTimestamp(), $this->to->getTimestamp());

        return (new DateTimeImmutable())->setTimestamp($i);
    }
}
