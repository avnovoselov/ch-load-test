<?php

namespace App\Infrastructure;

use ClickHouseDB\Client;
use ClickHouseDB\Exception\DatabaseException;
use Psr\Log\LoggerInterface;

class CHConnection
{
    private const CONFIG = [
        'host' => 'clickhouse',
        'port' => '8123',
        'username' => 'default',
        'password' => '',
    ];

    private const DATABASE = 'billing_report';

    private readonly Client $client;

    public function __construct(private readonly LoggerInterface $logger)
    {
        $this->client = $this->provideDbClient();
    }

    public function insert(string $tableName, array $rows, array $column)
    {
        $this->client->insert($tableName, $rows, $column);
    }

    public function select(string $statement)
    {
        return $this->client->select($statement);
    }

    private function provideDbClient(): Client
    {
        $client = new Client(self::CONFIG);

        try {
            $client->write(sprintf("CREATE DATABASE %s", self::DATABASE));
        } catch (DatabaseException) {
            $this->logger->info("Database already created");
        }
        try {
            $client->write(sprintf("CREATE TABLE %s.operations
                (
                    id           UInt64,
                    user_id      UInt64,
                    created_at   DATETIME,
                    amount       Int64,
                    income       BOOL
                )
                ENGINE = MergeTree PARTITION BY toMonth(created_at) ORDER BY (user_id, created_at);
            ", self::DATABASE));
        } catch (DatabaseException) {
            $this->logger->info("Table already created");
        }

        $client->database(self::DATABASE);

        return $client;
    }
}
